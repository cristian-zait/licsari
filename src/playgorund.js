
function add(x, y) {
	return x + y;
}

function multiply(x, y) {
	return x * y;
}

(function(){

	var x = 10;
	var y = 20;
	console.log("x = " + x + ", y = " + y);
	console.log("x + y = " + add(x, y));
	console.log("x * y = " + multiply(x, y))

})();