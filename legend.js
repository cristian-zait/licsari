var legend = [
  // standard js words
  { search: 'break', replace: '!LICSARI;;;' },
  { search: 'case', replace: '?liicsaarii' },
  { search: 'catch', replace: '!!!LICSARI!!!' },
  { search: 'else', replace: 'licSARI!!!' },
  { search: 'for', replace: 'LICSARI{}' },
  { search: 'function', replace: 'LiCsArILiCsArI' },
  { search: 'if', replace: 'LICsari!!!' },
  { search: 'instanceof', replace: 'licsarilicsarili' },
  { search: 'new', replace: 'LLLIIICCCSSSAAARRRIII!!!' },
  { search: 'return', replace: 'LICSARI::' },
  { search: 'switch', replace: 'LICSARILICSARI' },
  { search: 'throw', replace: '!l1csari!' },
  { search: 'try', replace: '!licsar1!' },
  { search: 'typeof', replace: 'Licsari?????????!?!?!?!?' },
  { search: 'var', replace: '$LICSARI:' },
  { search: 'while', replace: 'licsari............' },
  { search: 'console.log', replace: 'licsari.lics' },

  // most common letters that are not h, o, d or r
  // s a c m p t b f g i n e l w u v j k q y z x
  // "x "
  { search: 's', replace: 'Licsari ' },
  { search: 'd', replace: 'LICSARI ' },
  { search: 'c', replace: 'licsari ' },
  
  // "x. "
  { search: 'm', replace: 'Licsari. ' },
  { search: 'p', replace: 'LICSARI. ' },
  { search: 't', replace: 'licsari. ' },

  // "x! "
  { search: 'b', replace: 'Licsari! ' },
  { search: 'f', replace: 'LICSARI! ' },
  { search: 'g', replace: 'licsari! ' },

  // "x? "
  { search: 'o', replace: 'Licsari? ' },
  { search: 'n', replace: 'LICSARI? ' },
  { search: 'e', replace: 'licsari? ' },

  // "x!? "
  { search: 'h', replace: 'Licsari!? ' },
  { search: 'w', replace: 'LICSARI!? ' },
  { search: 'u', replace: 'licsari!? ' },

  // "x?! "
  { search: 'v', replace: 'Licsari?! ' },
  { search: 'j', replace: 'LICSARI?! ' },
  { search: 'k', replace: 'licsari?! ' },

  // "x!?! "
  { search: 'q', replace: 'Licsari!?! ' },
  { search: 'y', replace: 'LICSARI!?! ' },
  { search: 'z', replace: 'licsari!?! ' },

  // "x?!? "
  { search: 'x', replace: 'Licsari?!? ' },
  { search: 'S', replace: 'LICSARI?!? ' },
  { search: 'D', replace: 'licsari?!? ' },

  // "x... "
  { search: 'C', replace: 'Licsari... ' },
  { search: 'M', replace: 'LICSARI... ' },
  { search: 'P', replace: 'licsari... ' },

  // "x-"
  { search: 'T', replace: 'Licsari- ' },
  { search: 'B', replace: 'LICSARI- ' },
  { search: 'F', replace: 'licsari- ' },

  // "prepending Lii1csaaar1"
  { search: 'G', replace: 'Lii1csaaar1Licsari ' },
  { search: 'O', replace: 'Lii1csaaar1LICSARI ' },
  { search: 'N', replace: 'Lii1csaaar1licsari ' },

  // "prepending Liiicsariii"
  { search: 'E', replace: 'LiiicsariiiLicsari ' },
  { search: 'L', replace: 'LiiicsariiiLICSARI ' },
  { search: 'W', replace: 'Liiicsariiilicsari ' },

  // "appending Liiicsariii"
  { search: 'U', replace: 'LicsariLiiicsariii ' },
  { search: 'V', replace: 'LICSARILiiicsariii ' },
  { search: 'J', replace: 'licsariLiiicsariii ' },

  // "appending Lii1csaaar1"
  { search: 'K', replace: 'LicsariLii1csaaar1 ' },
  { search: 'Q', replace: 'LICSARILii1csaaar1 ' },
  { search: 'Y', replace: 'licsariLii1csaaar1 ' },

  // "prepending Lii1csaaar1Liiicsariii"
  { search: 'Z', replace: 'Lii1csaaar1LiiicsariiiLicsari ' },
  { search: 'X', replace: 'Lii1csaaar1LiiicsariiiLICSARI ' },
  //Add Hungarian unique characters to Licsari language  { search: 'á', replace: 'Hoodor ' },
  { search: 'Á', replace: 'LIicsari ' },
  { search: 'é', replace: 'Licsaari ' },
  { search: 'É', replace: 'LIcsaari ' },
  { search: 'ó', replace: 'Liiicsari ' },
  { search: 'Ó', replace: 'LIiicsari ' },
  { search: 'ü', replace: 'Licsaaari ' },
  { search: 'Ü', replace: 'LIcsaaari ' },
  { search: 'ú', replace: 'Liiiicsari ' },
  { search: 'Ú', replace: 'LIiiicsari ' },
  { search: 'ű', replace: 'Licsaaari ' },
  { search: 'Ű', replace: 'LIcsaaari ' },
  { search: 'í', replace: 'Liiiiicsaari ' },
  { search: 'Í', replace: 'LIiiiicsaari ' },
  { search: 'ő', replace: 'LiiiiicsaAAAari ' },
  { search: 'Ő', replace: 'LIiiiicsaAAAari ' },
  { search: 'ö', replace: 'LiiiiicsaAAAAari ' },
  { search: 'Ö', replace: 'LIiiiicsaAAAAari ' }
];

module.exports = legend;
