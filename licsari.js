#!/usr/bin/env node

/**
 * Module dependencies.
 */

String.prototype.replaceAll = function(substring, replacement) {
  var result = '';
  var lastIndex = 0;

  while(true) {
    var index = this.indexOf(substring, lastIndex);
    if(index === -1) break;
    result += this.substring(lastIndex, index) + replacement;
    lastIndex = index + substring.length;
  }

  return result + this.substring(lastIndex);
};

var program = require('commander'),
    pkginfo = require('pkginfo')(module, 'version'),
    colors  = require("colors"),
    fs      = require('fs');

var legend = require('./legend'); // ./ means current directory, and don't need .js b/c all require files are js

program
  .version(module.exports.version, '-v, --version')
  .description('Licsari licsari licsari')
  .parse(process.argv);

var licsari = program.args[0];

if( typeof(licsari) === 'undefined') {
  console.log('LICSARI:'.bold.red + ' licsari licsari licsari!'.red);
} else {
  if (licsari.search(".lcs") > 0) { // user entered a .hd file
    console.log('LICSARI: '.bold.cyan + '\\-> '.white + licsari.white);
    var text = fs.readFileSync(licsari).toString(); // the contents of the file
    convertCode(text);
  } else { // user entered something apart from a hd file
    console.log('LICSARI:'.bold.red + ' licsari licsari!'.red);
  }
}

function convertCode (text) {
  var licsariText = text;
  
  for (i = (legend.length - 1); i >= 0; i--){
    var query = legend[i];

    licsariText = licsariText.replaceAll(query.replace, query.search);
  }

  licsariText = licsariText.replace(/hello,? world!?/ig, 'BASTOS LICSARI!');

  eval(licsariText);
}