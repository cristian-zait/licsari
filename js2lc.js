#!/usr/bin/env node

/**
 * Module dependencies.
 */

var program = require('commander'),
    pkginfo = require('pkginfo')(module, 'version'),
    colors  = require("colors"),
    fs      = require('fs');
    glob    = require('glob');

var legend = require('./legend'); // ./ means current directory, and don't need .js b/c all require files are js

program
  .version(module.exports.version, '-v, --version')
  .description('Licsari licsari licsari')
  .parse(process.argv);

var licsari = program.args[0];

if( typeof(licsari) === 'undefined') {
  console.log('LICSARI:'.bold.red + ' licsari licsari licsari!'.red);
} else {
  if (licsari.search(".js") > 0) { // user entered a .js file
    glob(licsari, function(err, files) {
      files.forEach(file => {
        console.log('LICSARI: '.bold.cyan + file.bold.white + ' => '.yellow + file.replace('.js', '.lcs').bold.white);
        var text = fs.readFileSync(file).toString(); // the contents of the file
        convertCode(text, file);
      });
    });
  } else { // user entered something apart from a js file
    console.log('LICSARI:'.bold.red + ' licsari licsari!'.red);
  }
}

function convertCode (text, licsari) {
  var outputFileName = licsari.replace(".js", ".lcs");
  var licsariText = text;
  
  for (i = 0; i < legend.length; i++){
    var query = legend[i];
    var re    = new RegExp(query.search, 'g');

    licsariText = licsariText.replace(re, query.replace);
  }
  
  fs.writeFileSync(outputFileName, licsariText);  
}